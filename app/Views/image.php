<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>Image Manipulation</title>
  </head>
  <body>

  <div class="container">
    <h1>Image Manipulation</h1>
    <?php if(isset($validation)) : ?>
        <div class="text-danger">
            <?= $validation->listErrors()?>
        </div>
    <?php endif; ?>
  

    <form method="post" enctype="multipart/form-data">
    
    <div class="mb-3">
        <label for="formFile" class="form-label">Upload File</label>
        <input class="form-control" multiple name="theFile[]" type="file" id="formFile">
    </div>
   
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>  
  
  <?php if(isset($image)) : ?>
    <div class="row">
        <div class="col-md-4">
            <img src="<?= src($image)?>" alt="" class="img-fluid">        
        </div>
        <?php foreach ($folders as $folder) : ?> 
            <div class="col-md-4">
                <img src="<?= src($image, $folder)?>" alt="" class="img-fluid">
            </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>

    

    <!-- Optional JavaScript; choose one of the two! -->

    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
  </body>
</html>