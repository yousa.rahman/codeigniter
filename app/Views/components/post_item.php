<div class="card col-9 col-sm-5 col-md-3" style="width: 18rem;">
  <img src="/assets/11.jpg" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title"><?= $title?></h5>
    <p class="card-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Assumenda veniam mollitia cupiditate eius reiciendis molestias, eos distinctio repellendus laborum nam at repudiandae molestiae adipisci aspernatur beatae. Aut quas nulla repudiandae.</p>
    <a href="#" class="btn btn-primary">Read more</a>
  </div>
</div>

