<?= $this->extend('layouts/main')?>

<?= $this->section('content')?>
    <h2><?= $title?></h2>

<div class="row">
    <div class="col-12 col-md-8 offset-md-2">   
        <form  method="post">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" name="post_title" class="form-control" value="<?=$post['post_title']?>">
            </div>
            <div class="for-group">
                <label for="">Content</label>
                <textarea name="post_content" id=""  rows="3" class="form-control" ><?=$post['post_content']?></textarea>
            </div>
            <div class="form-group">
                <button class="btn btn-success btn-sm">Update</button>
            </div>
        </form>
    </div>
   
</div>
        
<?= $this->endSection()?>