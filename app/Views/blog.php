<?= $this->extend('layouts/main')?>

<?= $this->section('content')?>
    <h2><?= $title?></h2>

<div class="row">
    <?= $this->include('widgets/sidebar')?>
    
    <div class="row col-12 col-sm-9">
        <?php foreach ($posts as $post) : ?>
            <?= view_cell('\App\Libraries\Blog::postItem',['title'=> $post])?> 
        <?php endforeach; ?>
    </div>
</div>
        
<?= $this->endSection()?>