<?= $this->extend('layouts/main')?>

<?= $this->section('content')?>
    <h2><?= $title?></h2>
    <p class="h3"><?= $content?></p>
    <a href="/blog/delete/<?=$post['post_id']?>"><button class="btn btn-danger btn-sm">Delete</button></a>
    <a href="/blog/edit/<?=$post['post_id']?>"><button class="btn btn-primary btn-sm">Edit</button></a>
<?= $this->endSection()?>