<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>Form Validation</title>
  </head>
  <body>

  <div class="container">
    <h1>Form Validation</h1>
    <?php if(isset($validation)) : ?>
        <div class="text-danger">
            <?= $validation->listErrors()?>
        </div>
    <?php endif; ?>
  

    <form method="post" enctype="multipart/form-data">
    <div class="form-group mb-3">
        <label for="exampleInputEmail1">Email address</label>
        <input name="email" type="text" class="form-control" value="<?=set_value('email')?>"  id="exampleInputEmail1" aria-describedby="emailHelp">
        <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
    </div>
    <div class="mb-3">
        <label for="exampleInputPassword1" class="form-label">Password</label>
        <input name="password" type="password" value="<?=set_value('password')?>" class="form-control" id="exampleInputPassword1">
    </div>
    <div class="form-group">
        <label for="">Category</label>
        <option value=""></option>
        <select name="category" class="form-control">
            <?php foreach($categories as $cat) : ?>
            <option <?=set_select('category', $cat)?> value="<?=$cat?>"><?=$cat?></option>
            <?php endforeach;?>
        </select>
        <br>
    </div>
    <div class="form-group mb-3">
        <label for="date">Date</label>
        <input name="date" type="date" value="<?=set_value('date')?>" class="form-control" id="date">
    </div>
    <div class="mb-3">
        <label for="formFile" class="form-label">Upload File</label>
        <input class="form-control" multiple name="theFile[]" type="file" id="formFile">
    </div>
    <?php
        echo'<pre>';
        print_r($_POST);
        echo'<pre>';
    ?>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>  
  </div>
    

    <!-- Optional JavaScript; choose one of the two! -->

    
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
  </body>
</html>