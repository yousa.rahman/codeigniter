<?php namespace App\Models;

use CodeIgniter\Database\ConnectionInterface;

class CustomModel{

    protected $db;

    public function __construct(ConnectionInterface &$db){
        $this->db =& $db;
    }

    function all(){
        return $this->db->table('posts')->get()->getResult();
    }

    function where(){
        return $this->db->table('posts')
                        ->where(['post_id >='=> 100])
                        // ->where(['post_id >='=> 90])
                        ->orderBy('post_id','DESC')
                        ->get()
                        ->getRow();
    }

    function join(){
        return $this->db->table('posts')
                        ->where(['post_id >='=> 100])
                        ->join('users','posts.post_author = users.user_id')
                        ->get()
                        ->getResult();
    }

    function like(){
        return $this->db->table('posts')
                        ->like('post_content', 'new')
                        ->join('users','posts.post_author = users.user_id')
                        ->get()
                        ->getResult();
    }

    function grouping(){
        return $this->db->table('posts')
                        ->groupStart()
                            ->where(['post_id' => '25' , 'post_created_at <' => '1990-01-01 00:00:00'])
                        ->groupEnd()
                        ->join('users','posts.post_author = users.user_id')
                        ->get()
                        ->getResult();
    }

    function getPosts(){
        $builder = $this->db->table('posts');
        $builder->join('users','posts.post_author = users.user_id');
        $posts = $builder->get()->getResult();
        return $posts;
    }
    
    function getUsers($limit = false){
        $builder = $this->db->table('users');

        if($limit)
            $builder->limit($limit);

        $posts = $builder->get()->getResult();
        return $posts;
    }

    function getAnotherUsers($limit = false){
        $builder = $this->db->table('users');

        if($limit)
            $builder->limit($limit);

        $posts = $builder->get()->getResult();
        return $posts;
    }
}