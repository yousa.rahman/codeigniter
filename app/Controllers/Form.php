<?php namespace App\Controllers;

class Form extends BaseController
{
	public function index()
	{
		helper(['form']);

		$data = [];
		$data['categories'] = [
			'Student',
			'Teacher',
			'Principal'
		];

		
		if($this->request->getMethod() == 'post'){
			$rules = [
				// 'email' => [
				// 	'rules'=>'required|valid_email',
				// 	'label'=>'Email Address',
				// 	'errors'=>[
				// 		'required'=>'Email is super important',
				// 		'valid_email'=>'Check weather you entered a correct email'
				// 	]
				// ],
				// 'password' => 'required|min_length[8]',
				// 'date' => [
				// 	'rules'=>'required|check_date',
				// 	'errors'=>[
				// 		'required'=>'Date is super important',
				// 		'check_date'=>'Please enter the current date not a previous one'
				// 	]
				// ],
				'theFile'=> [
					'rules'=> 'uploaded[theFile.0]|is_image[theFile]',
					'lablel'=> 'theFile',

				]
			];
			if($this->validate($rules)){

				// $file = $this->request->getFile('theFile');
				// if($file->isValid() && !$file->hasMoved()){
				// 	$file->move('./uploads/images',$file->getRandomName());
				// }

				$files = $this->request->getFiles();
				foreach($files['theFile'] as $file){
					if($file->isValid() && !$file->hasMoved()){
						$file->move('./uploads/multiple',$file->getRandomName());
					}
	
				}
				
				return redirect()->to('/form/success');

			}else{
				$data['validation'] = $this->validator;
			}
		}

		return view('form',$data);
	}

	public function success(){
		return 'Hey you succcessfully passed the validation';
	}

	
}
