<?php namespace App\Controllers;

class Image extends BaseController
{
	public function index()
	{
		helper(['form','image']);

		$data = [];
				
		if($this->request->getMethod() == 'post'){
			$rules = [
				'theFile'=> [
					'rules'=> 'uploaded[theFile.0]|is_image[theFile]',
					'lablel'=> 'The File',

				]
			];
			if($this->validate($rules)){

				// $file = $this->request->getFile('theFile');
				// if($file->isValid() && !$file->hasMoved()){
				// 	$file->move('./uploads/images',$file->getRandomName());
				// }
				
				$path = './uploads/images/manipulated/';
				$files = $this->request->getFiles();
				$image = service('image');

				foreach($files['theFile'] as $file){
					if($file->isValid() && !$file->hasMoved()){

						$file->move($path);
						$fileName = $file->getName();
						$data['image'] = $fileName;

						$this->imageManipulation($path,'thumbs', $fileName, $image);
						$data['folders'][] = 'thumbs';
						$this->imageManipulation($path,'flip', $fileName, $image);
						$data['folders'][] = 'flip';
						

					}
	
				}
				
			}else{
				$data['validation'] = $this->validator;
			}
		}

		return view('image',$data);
	}

	private function imageManipulation($path, $folder, $fileName, $imageService)
	{
		$savePath = $path . '/' . $folder;
		if(!file_exists($savePath)) 
			mkdir($savePath, 755);

		$imageService->withFile(src($fileName));

		switch($folder){
			case 'thumbs':
				$imageService->fit(150,150);
				break;
			
			case 'flip':
				$imageService->flip('horizontal');
				break;
		}
		return $imageService->save($savePath.'/'.$fileName);
	}

	
}
